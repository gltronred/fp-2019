{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import qualified Test.Tasty.SmallCheck as SC

import Test.SmallCheck.Series

import Types

import MonadicExercises
import Data.Aeson (decode, encode, object, (.=))
import Data.ByteString.Lazy (ByteString)
import Text.Printf

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ unitTests
  , quickCheckTests
  , smallCheckTests
  , testMonadicExercises
  ]

unitTests :: TestTree
unitTests = testGroup "Unit Tests"
  [ testCase "2x2 == 4" $
    2*2 @?= 4
  , testCase "4 == 2x2" $
    4 @=? 2*2
  , testCase "len [] == 0" $
    lenWrong [] @?= 0
  , testCase "len [0,1,2] == 3" $
    lenWrong [1,2,0] @?= 3
  -- , testCase "len [0] == 1" $
  --   lenWrong [0] @?= 1
  ]

instance Arbitrary a => Arbitrary (BinTree a) where
  arbitrary = sized $ \n -> case n of
    0 -> pure Null
    1 -> pure Null
    _ | n < 0 -> pure Null
      | otherwise -> do
          k <- choose (1,n-1)
          Tree
            <$> resize k arbitrary
            <*> arbitrary
            <*> resize (n-1-k) arbitrary
  shrink Null = []
  shrink (Tree l x r) =
    [Null] ++
    [l,r] ++
    [ Tree l' x' r'
    | (l',x',r') <- shrink (l,x,r)]

instance Serial m a => Serial m (BinTree a) where
  series = cons0 Null \/ cons3 Tree

quickCheckTests =
  -- localOption (QuickCheckVerbose True) $
  testGroup "QC Properties"
  [ testProperty "len (a:as) > len as" $
    \a as -> lenWrong (a:as) > lenWrong as
  , testProperty "sum tree > sum left" $
    \left root right -> root > 0 ==> sumBinTree (Tree left (root :: Int) right) > sumBinTree left
  , testProperty "sum (l x r) == sum (r x l)" $
    \l x r -> sumBinTree (Tree l (x::Integer) r) === sumBinTree (Tree r x l)
  ]

smallCheckTests =
  -- localOption (QuickCheckVerbose True) $
  testGroup "SC Properties"
  [ SC.testProperty "len (a:as) > len as" $
    \a as -> lenWrong (a:as) > lenWrong as
  , localOption (SC.SmallCheckDepth 2) $
    SC.testProperty "sum tree > sum left" $
    \left root right -> root > 0 SC.==> sumBinTree (Tree left (root :: Int) right) > sumBinTree left
  , localOption (SC.SmallCheckDepth 2) $
    SC.testProperty "sum (l x r) == sum (r x l)" $
    \l x r -> sumBinTree (Tree l (x::Integer) r) == sumBinTree (Tree r x l)
  ]

testMonadicExercises = testGroup "Monadic exercises test"
  [ ok 2019 12 31
  , ok 2019 11 30
  , ok 2019  9 30
  , ok 2019  6 30
  , ok 2019  4 30
  , sf 2019 11 31
  , sf 2019  9 31
  , sf 2019  6 31
  , sf 2019  4 31
  , sf 2019  2 30
  , sf 2019  2 29
  , ok 2019  2 28
  , ok 2016  2 29
  , ok 2000  2 29
  , sf 1900  2 29
  , ok 1600  2 29
  , testProperty "All days > 0" $
    \y m (Negative d) -> wrong y m d
  , testProperty "All days <= 31" $
    \y m (Large d) -> d > 31 ==> wrong y m d
  , testProperty "All monthes > 0" $
    \y (Negative m) -> wrong y m 15
  , testProperty "All monthes <= 12" $
    \y (Large m) -> m > 12 ==> wrong y m 14
  ]
  where
    v :: Integer -> Int -> Int -> ByteString
    v y m d = encode $ object [ "year" .= y
                              , "month" .= m
                              , "day" .= d
                              ]
    t y m d rest = concat $ [show y, "-", printf "%02d" m, "-", printf "%02d" d] ++ rest
    ok y m d = testCase (t y m d $ pure " parses") $
      decode (v y m d) @?= Just (Date y m d)
    sf y m d = testCase (t y m d $ pure " fails") $
      decode (v y m d) @?= (Nothing :: Maybe Date)
    wrong y m d = decode (v y m d) === (Nothing :: Maybe Date)

