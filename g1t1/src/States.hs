module States where

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State

{-
f0 := 1;
f1 := 1;
f2 := f0+f1; // 2
f0 := f1;
f1 := f2;
f2 := f0+f1; // 3
-}
comp1 = let
  f0 = 1
  f1 = 1
  f2 = f0 + f1
  f0' = f1
  f1' = f2
  f2' = f0' + f1'
  f0'' = f1'
  f1'' = f2'
  f2'' = f0'' + f1''
  in f2''

data St s a = St { runSt :: s -> (s,a) }

getVar :: St s s
getVar = St $ \s -> (s,s)

putVar :: s -> St s ()
putVar new = St $ \_old -> (new, ())

instance Functor (St s) where
  fmap f (St g) = St $ fmap (fmap f) g

instance Applicative (St s) where
  pure x = St $ \s -> (s,x)
  St sf <*> St sa = St $ \s0 -> let
    (s1,f) = sf s0
    (s2,a) = sa s1
    in (s2, f a)

instance Monad (St s) where
  St sa >>= f = St $ \s0 -> let
    (s1,a) = sa s0
    St sb = f a
    in sb s1

comp2 :: St (Int,Int) Int
comp2 = do
  (f0, f1) <- getVar
  putVar (f1, f0+f1)
  (f0, f1) <- getVar
  putVar (f1, f0+f1)
  (f0, f1) <- getVar
  putVar (f1, f0+f1)
  pure $ f0+f1

comp3 :: St (Int,Int) ()
comp3 = replicateM_ 3 $ do
  (f0,f1) <- getVar
  putVar (f1, f0+f1)

-- Вычисление, возвращающее количество шагов до 1
-- Шаг:
--  * если текущее число x нечётное, заменить на 3x+1
--  * если x - чётное, заменить на x`div`2
data Collatz1 = Collatz1
  { currentNumber :: Integer
  , currentSteps :: Int
  } deriving (Eq,Show,Read)

collatz :: St Collatz1 Int
collatz = do
  s <- getVar
  if currentNumber s == 1
    then pure $ currentSteps s
    else do
    let f x | odd x = 3*x+1
            | otherwise = x`div`2
    putVar $ Collatz1
      { currentNumber = f $ currentNumber s
      , currentSteps = currentSteps s + 1
      }
    collatz

collatz1 :: State Collatz1 ()
collatz1 = do
  x <- gets currentNumber
  unless (x==1) $ do
    let f x | odd x = 3*x+1
            | otherwise = x`div`2
    modify' (\(Collatz1 x n) -> Collatz1 (f x) (n+1))
    collatz1

collatz2 :: StateT Collatz1 IO ()
collatz2 = do
  x <- gets currentNumber
  lift $ putStrLn $ "Current number: " ++ show x
  unless (x==1) $ do
    let f x | odd x = 3*x+1
            | otherwise = x`div`2
    modify' (\(Collatz1 x n) -> Collatz1 (f x) (n+1))
    collatz2

