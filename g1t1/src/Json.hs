{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Json where

import Data.Aeson
import Data.Aeson.Types
import GHC.Generics

data Point = Point
  { x :: Double
  , y :: Double
  } deriving (Eq,Show,Read,Generic)

-- instance ToJSON Point where
--   toJSON (Point x y) = object [ "x" .= x, "y" .= y ]
--   -- для более быстрого преобразования в JSON:
--   toEncoding (Point x y) = pairs ( "x" .= x <> "y" .= y )
-- 
-- instance FromJSON Point where
--   parseJSON (Object v) = Point <$> v .: "x" <*> v .: "y"
--   parseJSON other = typeMismatch "Point" other

instance FromJSON Point
instance ToJSON Point

data Figure
  = Circle
    { center :: Point
    , radius :: Double
    }
  | Rectangle
    { topLeft :: Point
    , bottomRight :: Point
    }
  deriving (Generic,Eq,Show,Read)

-- instance ToJSON Figure where
--   toJSON (Circle c r) = object [ "type" .= ("circle" :: String)
--                                , "center" .= c
--                                , "radius" .= r
--                                ]
--   toJSON (Rectangle tl br) = object [ "type" .= ("rectangle" :: String)
--                                     , "top-left" .= tl
--                                     , "bottom-right" .= br
--                                     ]
-- 
-- instance FromJSON Figure where
--   parseJSON (Object v) = do
--     type_ <- v .: "type"
--     case (type_ :: Maybe String) of
--       Just "circle" -> Circle <$> v.:"center" <*> v.:"radius"
--       Just "rectangle" -> Rectangle <$> v.:"top-left" <*> v.:"bottom-right"
--       _ -> fail "Can't parse Figure: type should be circle or rectangle"
--   parseJSON other = typeMismatch "Figure" other

instance FromJSON Figure where
  parseJSON = genericParseJSON defaultOptions
instance ToJSON Figure where
  toJSON = genericToJSON defaultOptions
  toEncoding = genericToEncoding defaultOptions

-- v .:? "optional-field" .!= defaultValue

