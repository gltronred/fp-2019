
{-# LANGUAGE OverloadedStrings #-}
module MonadicExercises where

import Control.Monad

import Data.Aeson
import Data.Aeson.Types (Parser)

data Date = Date
  { year :: Integer
  , month :: Int
  , day :: Int
  } deriving (Eq,Show,Read)

-- Parse Date from JSON:
-- { "year": 2019, "month": 12, "day": 31 }
--
-- Use Monad instance of Parser
-- Report errors using "fail"

-- Мы можем выполнять композицию вычислений в
-- монадах
--
-- Поэтому можем вытащить общие части в отдельные
-- функции
parseBetween v t max = do
  x <- v .: t
  when (x <= 0 || x > max) $ fail "Wrong: negative of too large"
  pure x

instance FromJSON Date where
  ----- Решение с аппликативным функтором работает
  ----- Но оно неверное - части парсятся
  ----- независимо друг от друга
  ----- Корректное решение использует монады для
  ----- указания зависимостей между частями
  -- parseJSON (Object v) = Date <$> v .: "year" <*> v .: "month" <*> v .: "day"
  parseJSON (Object v) = do
    y <- v .: "year"
    -- Здесь мы выбираем различные варианты
    -- дальнейших действий в зависимости от
    -- полученного значения (fail или возвращение
    -- результата)
    m <- parseBetween v "month" 12
    -- Далее - зависимости от года и месяца
    let leap = y `mod` 4 == 0 &&
               y `mod` 100 /= 0 ||
               y `mod` 400 == 0
        feb = if leap then 29 else 28
        maxD = [ 31, feb, 31
               , 30, 31, 30
               , 31, 31, 30
               , 31, 30, 31
               ] !! (m-1)
    d <- parseBetween v "day" maxD
    pure $ Date y m d
  parseJSON _ = fail "Date should be encoded as object"

-- eitherDecode "{\"year\":2019, \"month\":12, \"day\":31}" :: Either String Date

