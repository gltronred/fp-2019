{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Interpreter where

import Control.Monad
import Control.Monad.Free
import Control.Comonad.Cofree

import Data.Functor.Identity

data CmdF next
  = GetLine (String -> next)    -- s <- getLine
  | PutStrLn String next        -- putStrLn "asdasd"
  deriving (Functor)

-- instance Functor CmdF where
--   fmap f c = case c of
--     GetLine g -> GetLine $ fmap f g
--     PutStrLn s n -> PutStrLn s $ f n

type Cmd = Free CmdF

input :: Cmd String
input = liftF $ GetLine id

output :: String -> Cmd ()
output s = liftF $ PutStrLn s ()

interpret :: Cmd a -> IO a
interpret (Pure a) = pure a
interpret (Free f) = case f of
  GetLine next -> do
    s <- getLine
    interpret $ next s
  PutStrLn s next -> do
    putStrLn s
    interpret next

debug :: Cmd a -> String
debug (Pure a) = "pure"
debug (Free f) = case f of
  GetLine next -> "getLine\n" ++ debug (next "")
  PutStrLn s next -> "putStrLn \"" ++ s ++ "\"\n" ++ debug next

prog1 :: Cmd String
prog1 = do
  s <- input
  output s
  pure $ case s of
    [] -> "Anonymous"
    _ -> s

r1 = interpret prog1
r2 = debug prog1


data CounterF next
  = Add Int next
  | Clear next
  | Total (Int -> next)
  deriving (Functor)

add :: Int -> Free CounterF ()
add k = liftF $ Add k ()

clear :: Free CounterF ()
clear = liftF $ Clear ()

total :: Free CounterF Int
total = liftF $ Total id

double = do
  x <- total
  add x

prog3 :: Free CounterF Int
prog3 = do
  add 3
  double
  add 1
  total

prog4 = replicateM 3 prog3

prog2 :: Free CounterF Int
prog2 = do
  add 3
  clear
  add 2
  x <- total
  add x
  add 1
  total




data CoCtrF next = CoCtrF
  { addH :: Int -> next
  , clearH :: next
  , totalH :: (Int, next)
  } deriving (Functor)

count :: Int -> Cofree CoCtrF Int
count start = coiter next start
  where next w = CoCtrF (hAdd w) (hClear w) (hTotal w)
        hAdd w = \k -> w + k
        hClear w = start
        hTotal w = (w, w)

counterDebug :: Cofree CoCtrF String
counterDebug = coiter next start
  where start = ""
        next w = CoCtrF (hAdd w) (hClear w) (hTotal w)
        hAdd w = \k -> w ++ "add " ++ show k ++ "\n"
        hClear w = w ++ "clear\n"
        hTotal w = (0, w ++ "total -> 0\n")

-----------------------------------------------------------

class (Functor f, Functor g) => Pairing f g where
  pair :: (a -> b -> r) -> f a -> g b -> r

instance Pairing Identity Identity where
  pair p (Identity a) (Identity b) = p a b

instance Pairing ((->) r) ((,) r) where
  pair p f (r,b) = p (f r) b

instance Pairing ((,) r) ((->) r) where
  pair p (r,a) f = p a (f r)

instance Pairing f g => Pairing (Cofree f) (Free g) where
  pair p (a :< _) (Pure x) = p a x
  pair p (_ :< fs) (Free gs) = pair (pair p) fs gs

---------------------------------------------------------

instance Pairing CoCtrF CounterF where
  pair p (CoCtrF a _ _) (Add x next) = p (a x) next
  pair p (CoCtrF _ c _) (Clear next) = p c next
  pair p (CoCtrF _ _ t) (Total next) = pair p t next


r3 = pair const (count 0) prog2
r4 = pair const (count 1) prog2

r5 = pair const counterDebug prog2

