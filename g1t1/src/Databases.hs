
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Databases where

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.Logger (NoLoggingT)
import Conduit (ResourceT)
import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Person
    name String
    age Int Maybe
    deriving Show
BlogPost
    title String
    authorId PersonId
    deriving Show
|]

type DbMonad = ReaderT SqlBackend (NoLoggingT (ResourceT IO))

runDb :: DbMonad a -> IO a
runDb act = runSqlite "/tmp/db.sqlite" $ do
  runMigration migrateAll
  act

-- from persistent tutorial:
-- https://www.yesodweb.com/book/persistent
initDb :: DbMonad ()
initDb = do
  -- insertions
  johnId <- insert $ Person "John Doe" $ Just 35
  janeId <- insert $ Person "Jane Doe" Nothing

  -- references
  insert $ BlogPost "My fr1st p0st" johnId
  insert $ BlogPost "One more for good measure" johnId

  -- selects
  oneJohnPost <- selectList [BlogPostAuthorId ==. johnId] [LimitTo 1]
  liftIO $ print (oneJohnPost :: [Entity BlogPost])

  -- select by id
  john <- get johnId
  liftIO $ print (john :: Maybe Person)

-- esqueleto: complex queries for persistent
-- http://hackage.haskell.org/package/esqueleto-3.2.2/docs/Database-Esqueleto.html

-- opaleye: complex queries for postgres
-- tutorial: https://github.com/tomjaguarpaw/haskell-opaleye/blob/master/Doc/Tutorial/TutorialBasic.lhs

-- selda: mapping between types and db
-- tutorial: https://selda.link/tutorial/ch1-example-explained/

