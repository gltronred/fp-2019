{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module ApiWork where

import Control.Monad.IO.Class (liftIO)
import Data.Aeson (encodeFile,eitherDecodeFileStrict)
import Network.HTTP.Client (newManager, defaultManagerSettings)

import Servant.API
import Servant
import Servant.JS
import Servant.Client

import Json

{-
GET /figures
GET /figures/:id
GET /figures/:id/points
POST /figures
GET /points
-}
type MyApi = "figures" :> Get '[JSON] [Figure]
  :<|> "figures" :> Capture "id" Int :> Get '[JSON] Figure
  :<|> "figures" :> Capture "id" Int :> "points" :> Get '[JSON] [Point]
  :<|> "figures" :> ReqBody '[JSON] Figure :> PostCreated '[JSON] Int
  :<|> "points" :> Get '[JSON] [Point]

figures = [ Circle (Point 1.0 2.0) 3.0
          , Rectangle (Point 4.0 5.0) (Point 6.0 7.0)
          ]

server :: Server MyApi
server = getFigures
  :<|> getFigureById
  :<|> (fmap toPoints <$> getFigureById)
  :<|> appendFigure
  :<|> (concatMap toPoints <$> getFigures)

getFigures :: Handler [Figure]
getFigures = do
  eres <- liftIO $ eitherDecodeFileStrict "state.json"
  case eres of
    Left _ -> throwError err500
    Right res -> pure res

getFigureById :: Int -> Handler Figure
getFigureById k = do
  fs <- getFigures
  if k < length fs
    then pure $ fs!!k
    else throwError err404

appendFigure :: Figure -> Handler Int
appendFigure f = do
  figures <- getFigures
  liftIO $ encodeFile "state.json" $ figures ++ [f]
  pure $ length figures

toPoints :: Figure -> [Point]
toPoints Circle{ center=c } = [c]
toPoints Rectangle { topLeft=tl, bottomRight=br } = [tl, br]

-- свидетельство (witness) для вашего Api
myApi :: Proxy MyApi
myApi = Proxy

-- wai - web application interface
-- Application - тип приложения в wai
app :: Application
app = serve myApi server

jsText = jsForAPI myApi vanillaJS

getFs :<|> getFsId :<|> getFsIdPs :<|> postFs :<|> getPs = client myApi

runClient :: ClientM a -> IO (Either ClientError a)
runClient act = do
  manager' <- newManager defaultManagerSettings
  runClientM act (mkClientEnv manager' (BaseUrl Http "localhost" 8081 ""))

