module UseParsec where

import Data.Char
import Text.Parsec

-------------------------------------------------------------

type Parser = Parsec String ()

myDigit :: Parser Int
myDigit = (\c -> ord c - ord '0') <$> satisfy isDigit
  where isDigit c = '0' <= c && c <= '9'

myNumber :: Parser Int
myNumber = foldl (\acc d -> 10*acc + d) 0 <$> many myDigit

csv :: Parser [[String]]
csv = row `sepBy` newline
  where row :: Parser [String]
        row = many (satisfy (\c -> c/=';' && c/='\n')) `sepBy` char ';'

elt :: Parser (Either Int String)
elt = optionMaybe (char '"') >>= \mq -> case mq of
  Nothing -> Left . read <$> many digit
  Just _ -> Right <$> many (satisfy (/='"')) <* char '"'

elt2 :: Parser (Either Int String)
elt2 = do
  mOpeningQuote <- optionMaybe $ char '"'
  case mOpeningQuote of
    Nothing -> do
      numS <- many digit
      pure $ Left $ read numS
    Just _ -> do
      str <- many $ satisfy (/='"')
      closingQuote <- char '"'
      pure $ Right str

workingWithIO :: IO ()
workingWithIO = do
  name <- getLine
  let str = if name == "" then "Anonymous" else name
  putStrLn $ "Hello, " ++ str

-- Парсинг языка со следующим синтаксисом:
--
-- Программа - последовательность операторов, разделённых точкой с запятой
-- Оператор ::=
--    <переменная> := <значение>
-- Переменная - последовательность букв и цифр, начинающаяся с буквы
-- Значение ::=
--    "<строка>" | <число>
-- Строка -- произвольная последовательность символов, не содержащая кавычек
-- Число -- произвольное неотрицательное число

data Prog = Prog { getOperators :: [Op] } deriving (Show,Read)
data Op = Op { lhs :: String, rhs :: Val } deriving (Show,Read)
data Val = I Int | S String deriving (Show,Read)

prog :: Parser Prog
prog = Prog <$> op `sepBy` char ';'

op :: Parser Op
op = do
  spaces
  var <- variable
  spaces
  string ":="
  spaces
  val <- value
  spaces
  pure $ Op var val

variable :: Parser String
variable = (:) <$> letter <*> many alphaNum

value :: Parser Val
value = sval <|> ival
  where ival = I . read <$> many digit
        -- sval = S <$> (char '"' *> many (satisfy (/='"')) <* char '"')
        sval = do
          char '"'
          str <- many $ satisfy (/='"')
          char '"'
          pure $ S str

