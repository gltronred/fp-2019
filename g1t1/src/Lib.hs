module Lib where

import Types
import Recursion

-- функции высшего порядка
twice f x y z = f (f x y) z

-- функции могут быть объявлены в произвольном порядке
c1 = twice sillyFunc 1 2 3

c2 = sillyFunc 4

cnst' x y = y

c3 = cnst' 1






-- (необязательные) объявления типов
f :: Integer
f = 1

someFunc :: IO ()
someFunc = putStrLn "someFunc"

-- арифметика
sillyFunc x y = x + y*y

cnst x y = x

s1 x f = f + x

-- условные выражения
s2 x = if x > 0 then x else -x 

b p x y = if p then x else y

-----------------------------
-- our version of &&
(&&&) x y = if x then True else if y then True else False

{-
 Multiline comment:
 рекурсия
-}
myEven x = if x == 0 
           then True
           else if x == 1 
                then False
                else myOdd (x-1)
myOdd x = if x == 1 then True else if x == 0 then False else myEven (x-1)

factorial n = if n <= 1
              then 1
              else n * factorial (n-1)

-- локальные объявления
fib n = fibHelper n 1 1
  where
    fibHelper n a b = if n == 0 then a else if n == 1 then b else fibHelper (n-1) b (a+b)

-- сопоставление с образцом
f2 0 a b = a
f2 1 a b = b
f2 2 a b
  | a==b = a
  | a==2 = b
f2 n a b = f2 (n-1) b (a+b)

-- оно же, в виде case
f2' x a b = case x of
  0 -> a
  1 -> b
  2 | a == b -> a
    | a == 2 -> b
  n -> f2 (n-1) b (a+b)

