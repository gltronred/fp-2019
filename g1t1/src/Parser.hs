module Parser where

import Control.Applicative
import Control.Monad

import Data.Char

------------------------------------------------------------------
-- Парсеры как аппликативные функторы, монады
--

data P a = P { runParser :: String -> [(String, a)] }

-- Парсер любого символа
anyChar :: P Char
anyChar = P $ \s -> case s of
  [] -> []
  (x:xs) -> [(xs,x)]

-- Символ, удовлетворяющий условию
satisfy p = P $ \s -> case s of
  [] -> []
  (x:xs)
    | p x -> [(xs, x)]
    | otherwise -> []

-- Парсер конкретного символа
char :: Char -> P Char
char c = satisfy (==c)

-- Парсер является функтором
instance Functor P where
  ---- String->, Maybe, (String,) - функторы, поэтому так:
  fmap f (P g) = P $ fmap (fmap $ fmap f) g
  ---- либо всё вручную:
  -- fmap f (P g) = P $ \s -> case g s of
  --   Nothing -> Nothing
  --   Just (t,a) -> Just (t,f a)

-- (<$>) = fmap

digitVal :: P Int
digitVal = (\c -> ord c - ord '0') <$> satisfy isDigit
  where isDigit c = '0' <= c && c <= '9'

------------ Аппликативный функтор
--
-- class Functor f => Applicative (f :: * -> *) where
--   pure :: a -> f a
--   (<*>) :: f (a -> b) -> f a -> f b

-- Законы аппликативного функтора:
-- identity
--     pure id <*> v = v
-- composition
--     pure (.) <*> u <*> v <*> w = u <*> (v <*> w)
-- homomorphism
--     pure f <*> pure x = pure (f x)
-- interchange
--     u <*> pure y = pure ($ y) <*> u

instance Applicative P where
  pure x = P $ \s -> [(s,x)]
  P pf <*> parserA = P $ \s ->
    concatMap (\(t,f) -> fmap (fmap f) $ runParser parserA t) $ pf s

-- class Applicative f => Alternative (f :: * -> *) where
--   empty :: f a
--   (<|>) :: f a -> f a -> f a

instance Alternative P where
  empty = P $ \s -> []
  P pa <|> P pb = P $ \s -> case pa s of
    [] -> pb s
    res -> res

myMany1 :: P a -> P [a]
myMany1 pa = ((:) <$> pa <*> myMany pa) <|> pure []

myMany :: P a -> P [a]
myMany pa = mySome pa <|> pure []

mySome :: P a -> P [a]
mySome pa = ((:) <$> pa <*> myMany pa)

number :: P Int
number = read <$> many digit
  where digit = satisfy $ \c -> '0' <= c && c <= '9'

csv :: P [[String]]
csv = (:) <$> row <*> many (char '\n' *> row)

row :: P [String]
row = (:) <$> str <*> many (char ';' *> str)
  where str = many $ satisfy $ \c -> c /= ';' && c /= '\n'

---------------------- Монады
--
-- class Applicative m => Monad (m :: * -> *) where
--   (>>=) :: m a -> (a -> m b) -> m b

instance Monad P where
  P pa >>= f = P $ \s ->
    concatMap (\(t,a) -> runParser (f a) t) $ pa s

-- Символ, удовлетворяющий условию
satisfy2 p = anyChar >>= \c -> if p c
                               then pure c
                               else P $ const []

-- Завершается успешно, если строка закончилась
eof :: P ()
eof = P $ \s -> case s of
  [] -> [([], ())]
  _ -> []

-- Строка ровно из трёх символов
str3 :: P [Char]
str3 = replicateM 3 anyChar <* eof

-- Пример использования монады
strangeIdent :: P (Either Int String)
strangeIdent = anyChar >>= \c -> case c of
  'n' -> Left <$> number
  's' -> Right <$> many anyChar
  _ -> empty

strangeIdent2 :: P (Either Int String)
strangeIdent2 = do
  c <- anyChar
  case c of
    'n' -> Left <$> number
    's' -> do
      str <- many anyChar
      pure $ Right str
    _ -> empty

