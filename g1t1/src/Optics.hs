-- http://hackage.haskell.org/package/lens
-- https://artyom.me/lens-over-tea-1

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}

module Optics where

import Data.Functor.Compose
import Data.Functor.Const
import Data.Functor.Identity

data Users = Users
  { firstPlayer :: String
  , secondPlayer :: Maybe String
  } deriving (Eq,Show,Read)

data SomeGameState = SomeGameState
  { usersInGame :: Users
  , moves :: [Int]
  } deriving (Eq,Show,Read)

initialGameState = SomeGameState
  { usersInGame = Users
    { firstPlayer = "Alice"
    , secondPlayer = Nothing
    }
  , moves = []
  }

-- initialGameState { usersInGame = (usersInGame initialGameState) { secondPlayer = Just "Bob" }}

type Lens1 s a = forall f. Functor f => (a -> f a) -> s -> f s

ix :: Int -> Lens1 [a] a
ix index f list
  | index < 0        = error "ix: negative index"
  | null list        = error "ix: index too large"
  | old:rest <- list = if index == 0
                         then (: rest) <$> f old
                         else (old :) <$> ix (index-1) f rest

type Lens s t a b = forall f. Functor f => (a -> f b) -> s -> f t
type Lens' s a = Lens s s a a

-- _1 :: Functor f => (a -> f b) -> (a, x) -> f (b, x)
_1 :: Lens (a, x) (b, x) a b
_1 f (a,x) = (,x) <$> f a

-- _2 :: Functor f => (a -> f b) -> (x, a) -> f (x, b)
_2 :: Lens (x, a) (x, b) a b
_2 f (x,a) = (x,) <$> f a

-- Make a lens out of a getter and a setter.
lens :: (s -> a) -> (s -> b -> t) -> Lens s t a b
lens get set f s = set s <$> (f $ get s)

users :: Lens' SomeGameState Users
users = lens usersInGame (\s b -> s { usersInGame = b })

secondPl :: Lens' Users (Maybe String)
secondPl = lens secondPlayer (\s b -> s { secondPlayer = b })

-- -- Combine 2 lenses to make a lens which works on Either. (It's a good idea
-- -- to try to use bimap for this, but it won't work, and you have to use
-- -- explicit case-matching. Still a good idea, tho.)
-- choosing :: Lens s1 t1 a b -> Lens s2 t2 a b
--          -> Lens (Either s1 s2) (Either t1 t2) a b
-- choosing l1 l2 = _
-- 
-- -- Modify the target of a lens and return the result. (Bonus points if you
-- -- do it without lambdas and defining new functions. There's also a hint
-- -- before the end of the section, so don't scroll if you don't want it.)
-- (<%~) :: Lens s t a b -> (a -> b) -> s -> (b, t)
-- (<%~) l f s = _
-- 
-- -- Modify the target of a lens, but return the old value.
-- (<<%~) :: Lens s t a b -> (a -> b) -> s -> (a, t)
-- (<<%~) l f s = _
-- 
-- -- There's a () in every value. (No idea what this one is for, maybe it'll
-- -- become clear later.)
-- united :: Lens' s ()
-- united = _

---- Использование

-- > initialGameState
-- SomeGameState {usersInGame = Users {firstPlayer = "Alice", secondPlayer = Nothing}, moves = []}
-- > initialGameState & users . secondPl .~ Just "Bob"
-- SomeGameState {usersInGame = Users {firstPlayer = "Alice", secondPlayer = Just "Bob"}, moves = []}

---- Как устроено
-- > :t users . secondPl
-- users . secondPl
--   :: Functor f =>
--      (Maybe String -> f (Maybe String))
--      -> SomeGameState -> f SomeGameState

---- Что такое (.~)  (он же - set)

-- > runIdentity $ (users . secondPl) (const $ Identity $ Just "Bob") initialGameState
-- SomeGameState {usersInGame = Users {firstPlayer = "Alice", secondPlayer = Just "Bob"}, moves = []}

---- Получение значений из структуры
-- > initialGameState ^. users . secondPl
-- Nothing
-- > let secondState = initialGameState & users . secondPl .~ Just "Bob"
-- > secondState^.users.secondPl
-- Just "Bob"

---- Что такое (^.)  (он же - view)
-- > (users.secondPl) (\x -> (x,x)) secondState
-- (Just "Bob",SomeGameState {usersInGame = Users {firstPlayer = "Alice", secondPlayer = Just "Bob"}, moves = []})
-- > fst $ (users.secondPl) (\x -> (x,x)) secondState
-- Just "Bob"

