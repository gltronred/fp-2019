module Dining where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad

thread1 :: TBQueue String -> IO ()
thread1 q = atomically $ writeTBQueue q "Hello world"

thread2 :: TBQueue String -> IO ()
thread2 q = atomically $ writeTBQueue q "of concurrency!"

takeForks :: TMVar () -> TMVar () -> STM ()
takeForks left right = do
  () <- takeTMVar left
  () <- takeTMVar right
  -- -- Compilation error:
  -- putStrLn "Missiles are launched"
  pure ()

putForks :: TMVar () -> TMVar () -> STM ()
putForks left right = do
  putTMVar left ()
  putTMVar right ()

withFork :: TBQueue String -> String -> TMVar () -> STM a -> STM a
withFork q s f act = do
  () <- takeTMVar f
  writeTBQueue q $ "Took " ++ s
  a <- act
  writeTBQueue q $ "Acted " ++ s
  putTMVar f ()
  writeTBQueue q $ "Put " ++ s
  pure a

ph :: TBQueue String -> Int -> TMVar () -> TMVar () -> IO ()
ph q ident left right = atomically $ do
  -- writeTBQueue q $ "#" ++ show ident ++ " is starving!"
  -- takeForks left right
  -- writeTBQueue q $ "#" ++ show ident ++ " is eating!"
  -- putForks left right
  withFork q (show ident ++ " left") left $ do
    withFork q (show ident ++ " right") right $ do
      writeTBQueue q $ "#" ++ show ident ++ " is eating!"

dining :: IO ()
dining = do
  messages <- newTBQueueIO 100
  -- [f1,f2,f3,f4,f5] <- replicateM 5 $ atomically $ newTMVar ()
  [f1,f2,f3,f4,f5] <- atomically $ replicateM 5 $ newTMVar ()
  forkIO $ ph messages 1 f1 f2
  forkIO $ ph messages 2 f2 f3
  forkIO $ ph messages 3 f3 f4
  forkIO $ ph messages 4 f4 f5
  forkIO $ ph messages 5 f5 f1
  forever $ do
    msg <- atomically $ readTBQueue messages
    putStrLn msg

