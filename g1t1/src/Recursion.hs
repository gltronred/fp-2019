{-# LANGUAGE DeriveFunctor #-}

module Recursion where

import Control.Category
import Data.Functor.Foldable

-- https://blog.sumtypeofway.com/an-introduction-to-recursion-schemes/

type Lit = String

data Expr
  = Index Expr Expr             -- a[i]
  | Call Expr [Expr]            -- вызов функции
  | Unary String Expr           -- встроенная операция с одним аргументом
  | Binary Expr String Expr     -- ... с двумя аргументами
  | Paren Expr                  -- выражение в скобках
  | Literal Lit                 -- переменная
  deriving (Show, Eq)

-- this would turn the expression
--    (((anArray[(10)])))
-- into
--    anArray[10]

flatten :: Expr -> Expr
-- base case: do nothing to literals
flatten (Literal i) = Literal i

-- this is the important case: we shed the Paren constructor and just
-- apply `flatten` to its contents
flatten (Paren e) = flatten e

-- all the other cases preserve their constructors and just apply
-- the flatten function to their children that are of type `Expr`.
flatten (Index e i)     = Index (flatten e) (flatten i)
flatten (Call e args)   = Call (flatten e) (map flatten args)
flatten (Unary op arg)  = Unary op (flatten arg)
flatten (Binary l op r) = Binary (flatten l) op (flatten r)

applyExpr :: (Expr -> Expr) -> Expr -> Expr
-- base case: applyExpr is the identity function on constants
applyExpr f (Literal i) = Literal i
-- recursive cases: apply f to each subexpression
applyExpr f (Paren p) = Paren (f p)
applyExpr f (Index e i) = Index (f e) (f i)
applyExpr f (Call e args) = Call (f e) (map f args)
applyExpr f (Unary op arg) = Unary op (f arg)
applyExpr f (Binary l op r) = Binary (f l) op (f r)

flatten' (Paren x) = flatten' x
flatten' x = applyExpr flatten' x

data ExprF a
  = IndexF a a                   -- a[i]
  | CallF a [a]                  -- вызов функции
  | UnaryF String a              -- встроенная операция с одним аргументом
  | BinaryF a String a           -- ... с двумя аргументами
  | ParenF a                     -- выражение в скобках
  | LiteralF Lit                 -- переменная
  deriving (Show, Eq, Functor)

newtype Term f = In { out :: f (Term f) }

type Expr1 = Term ExprF

bottomUp :: Functor f => (Term f -> Term f) -> Term f -> Term f
bottomUp fn = out >>> fmap (bottomUp fn) >>> In >>> fn

removeParen (In (ParenF e)) = e
removeParen other = other

flatten'' = bottomUp removeParen


expr :: Expr1
expr = In (BinaryF (In (LiteralF "a")) "+" (In (UnaryF "f" (In (LiteralF "b")))))

-- catamorphism
myCata :: Functor f => (f a -> a) -> Term f -> a
myCata f = out >>> fmap (myCata f) >>> f

countNodes :: ExprF Int -> Int
countNodes (IndexF a1 a2) = 1 + a1 + a2
countNodes (CallF a1 as) = 1 + a1 + sum as
countNodes (UnaryF _ a) = 1 + a
countNodes (BinaryF a1 _ a2) = 1 + a1 + a2
countNodes (ParenF a) = 1 + a
countNodes (LiteralF _) = 1

-- anamorphism
myAna :: Functor f => (a -> f a) -> a -> Term f
myAna f = In <<< fmap (myAna f) <<< f

sumList :: [Int] -> Int
sumList = cata $ \list -> case list of
  Nil -> 0
  Cons a b -> a + b

unfoldList :: Int -> [Int]
unfoldList = ana $ \x -> if x <= 0
                         then Nil
                         else Cons x (x-1)



