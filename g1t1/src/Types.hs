module Types where

-- Получить тип выражения
-- :t выражение
-- Тип с заменой на типы по умолчанию
-- :t +d выражение

-- :i

data Sum1
  = A
  | B Int
  | C Bool
  | D Int

data Prod1 = P Int Bool

-- data Current
--   = AC Int Double
--   | DC Int
data Current
  = AC { voltage :: Int, current :: Double }
  | DC { voltage :: Int }
  deriving (Show,Read)

myVoltage :: Current -> Int
myVoltage (AC v _) = v
myVoltage (DC v) = v

-- сопоставление только с частью полей
myVoltage2 :: Current -> Int
myVoltage2 (AC { voltage=v }) = v
myVoltage2 DC {} = 0

-- data Maybe a
--   = Nothing
--   | Just a

fromMaybe :: a -> Maybe a -> a
fromMaybe def Nothing = def
fromMaybe _ (Just a) = a

-- Информация о виде типа
-- :kind, :k Maybe

-- Информация о типе, конструкторе и т.д.
-- :info, :i Maybe

-- Тип-сумма
-- data Either a b
--   = Left a
--   | Right b

-- Тип с единственным значением
-- unit type ()

maybeToEitherUnit :: Maybe a -> Either () a
maybeToEitherUnit Nothing = Left ()
maybeToEitherUnit (Just a)= Right a

eitherUnitToMaybe :: Either () b -> Maybe b
eitherUnitToMaybe (Left ()) = Nothing
eitherUnitToMaybe (Right b) = Just b

-- Адрес чата и репозитория ;)
chat, repo :: String
chat = "https://t.me/joinchat/DIb-HE9T_BLoBaT68j3L5A"
repo = "https://bitbucket.org/gltronred/fp-2019"

-- Тип-произведение - пара
-- (a,b)
-- (1,"a")

-- Вместо тысячи скобок...
-- ($) :: (a -> b) -> a -> b
-- -- f $ x = f x
-- ($) f x = f x

-- Функция id
-- id :: a -> a
-- id x = x

-- Композиция функций
-- (.) :: (b -> c) -> (a -> b) -> a -> c
-- (.) f g x = f (g x)

data List a
  = Nil
  | Cons a (List a)

-- Использование нашего типа
-- Nil
-- Cons 1 Nil
-- Cons 2 $ Cons 1 Nil
-- Cons 2 (Cons 1 Nil)
-- 1 `Cons` Nil
-- 2 `Cons` (1 `Cons` Nil)

-- Стандартный тип "список"
-- data [a] = [] | a : [a]

-- Длина списка. Библиотечная функция - length
len :: [a] -> Int
len [] = 0
len (_ : as) = 1 + len as

-- конкатенация двух списков
-- append [3,1,2] [4,5,6] == [3,1,2,4,5,6]
append :: [a] -> [a] -> [a]
append []     bs = bs
append (a:as) bs = a : append as bs

append2 :: [a] -> ([a] -> [a])
append2 [] = id
append2 (a : as) = (a:) . append2 as

-- (++) - конкатенация

-- подаются два упорядоченных по возрастанию списка целых
-- слить их в один упорядоченный по возрастанию список
-- merge [1,2,4] [0,3,9] == [0,1,2,3,4,9]
merge :: [Int] -> [Int] -> [Int]
merge []     bList = bList
merge aList  []    = aList
merge (a:as) (b:bs)
  | a < b     = a : merge as (b:bs)
  | otherwise = b : merge (a:as) bs

-- разделить список на два, примерно совпадающих по длине
-- например, первый, третий, пятый, ... элементы - в первый список,
-- остальные - во второй
-- divide [5,2,4,3,1] = ([5,4,1], [2,3])
divide :: [Int] -> ([Int], [Int])
divide [] = ([], [])
divide [a] = ([a], [])
divide (a : b : cs) = (a : firstHalf, b : secondHalf)
  where (firstHalf, secondHalf) = divide cs

-- Сортировка слиянием
mergeSort :: [Int] -> [Int]
mergeSort as
  | length as < 2 = as
  | otherwise = merge xs ys
  where (bs, cs) = divide as
        xs = mergeSort bs
        ys = mergeSort cs

-- Тип-дерево
data Tree a
  = Leaf a                      -- Лист
  | Node a [Tree a]             -- Промежуточный узел с поддеревьями
  deriving (Show,Read)          -- Стандартные экземпляры стандартных классов

-- высота дерева
height :: Tree a -> Int
height (Leaf _) = 1
height (Node _ ts) = 1 + maximum (map height ts)

-- количество элементов
count :: Tree a -> Int
count (Leaf _) = 1
count (Node _ ts) = 1 + sum (map count ts)

-- обход в порядке "левое поддерево - корень - правое поддерево"
-- inorder (Node 1 [Leaf 2, Node 3 [Leaf 4, Leaf 5]]) == [2, 1, 4, 3, 5]
inorder :: Tree a -> [a]
inorder (Leaf x) = [x]
inorder (Node root (left:rights)) = inorder left ++ [root] ++ concat (map inorder rights)
-- если бы в задании было указано, что дерево двоичное
-- и было указано, как кодируются деревья с одним поддеревом, тогда:
-- inorder (Node root []) = [root]
-- inorder (Node root [left]) = inorder left ++ [root]
-- inorder (Node root [left,right]) = inorder left ++ [root] ++ inorder right

-- есть ли элемент в дереве (не обязательно дерево поиска!)
-- элементы можно сравнивать на равенство при помощи (==) и (/=)
search :: Eq a => a -> Tree a -> Bool
search needle (Leaf x) = x == needle
search needle (Node root ts) = root == needle || any (search needle) ts

-----------------------------------------------------------------------------------------
-- Классы типов

-- стандартные:
-- Eq - проверка на равенство,
-- Ord - сравнение,
-- Num - арифметика,
-- Show - преобразование в строку,
-- Read - преобразование из строки

instance Eq Current where
  AC v1 c1 == AC v2 c2 = v1 == v2 && c1 == c2
  DC v1 == DC v2 = v1 == v2
  _ == _ = False

instance Eq a => Eq (Tree a) where
  Leaf x == Leaf y = x == y
  Node r1 ts1 == Node r2 ts2 = r1 == r2 && ts1 == ts2
  _ == _ = False

-- информация о классе типов:
-- :info Eq

-- собственные классы:
class PrettyPrint a where
  pprintRows :: a -> [(Int,String)]
  pprint :: a -> String
  pprint x = concat $ map (\(indent,row) -> replicate indent ' ' ++ row ++ "\n") $ pprintRows x

instance PrettyPrint Int where
  pprintRows x = [(0,show x)]

instance PrettyPrint a => PrettyPrint [a] where
  pprintRows xs = (0,"List of:") : map (\(i,s) -> (i+2,s)) (concatMap pprintRows xs)
-- *Main Lib Types> pprint [1,2,3 :: Int]
-- "List of:\n  1\n  2\n  3\n"
-- *Main Lib Types> putStrLn $ pprint [1,2,3 :: Int]
-- List of:
--   1
--   2
--   3

instance (PrettyPrint a, PrettyPrint b) => PrettyPrint (a,b) where
  pprintRows (a,b) = (0, "Pair of:") : map (\(i,s) -> (i+2,s)) (pprintRows a ++ pprintRows b)

instance PrettyPrint a => PrettyPrint (Tree a) where
  pprintRows (Leaf x) = let
    ((indent,firstRow) : rest) = pprintRows x
    in (indent, "- " ++ firstRow) : map (\(i,s) -> (i+2,s)) rest
  pprintRows (Node x ts) = let
    ((indent,firstRow) : rest) = pprintRows x
    (_header : subTreeRows) = pprintRows ts
    in (indent, "+ " ++ firstRow) : map (\(i,s) -> (i+2,s)) (rest ++ subTreeRows)

---------------------------------------------------------------------------------------------
-- Функторы, аппликативные функторы, монады

-- class Functor (f :: * -> *) where
--   fmap :: (a -> b) -> f a -> f b

-- Законы для функтора:
--   fmap id  ==  id
--   fmap (f . g)  ==  fmap f . fmap g

-- для списка:
-- fmap :: (a -> b) -> [a] -> [b]
-- для Either c
-- fmap :: (a -> b) -> Either c a -> Either c b
-- для (c,)
-- fmap :: (a -> b) -> (c,a) -> (c,b)
-- для Maybe
-- fmap :: (a -> b) -> Maybe a -> Maybe b

fmapList :: (a -> b) -> [a] -> [b]
fmapList f [] = []
fmapList f (a : as) = f a : fmapList f as
{- Доказательство fmap id == id
Докажем, что для произвольного xs: fmap id xs == id xs

Возможны два случая:
1. xs == []
1.1. fmap id [] == [] == id []
2. xs == (a : as)
2.1. Предположим, что для всех списков as длины меньше k выполняется fmap id as == id as
2.2. Докажем, что тогда для xs длины k выполняется fmap id xs == id xs
2.3. fmap id xs == fmap id (a:as) == id a : fmap id as
2.4. По предположению fmap id as == id as == as
2.5. Следовательно, id a : fmap id as == id a : id as == a : as == xs == id xs
-}

fmapEither :: (a -> b) -> Either c a -> Either c b
fmapEither f (Left c) = Left c
fmapEither f (Right a) = Right $ f a

fmapMaybe :: (a -> b) -> Maybe a -> Maybe b
fmapMaybe f Nothing = Nothing
fmapMaybe f (Just a) = Just $ f a

fmapPair :: (a -> b) -> (c,a) -> (c,b)
fmapPair f (c,a) = (c,f a)

-- Functor (r->...)
fmapFun :: (a -> b) -> (r -> a) -> (r -> b)
-- fmapFun f g r = f (g r)
-- fmapFun f g = f . g
fmapFun = (.)

instance Functor Tree where
  fmap f (Leaf x) = Leaf $ f x
  fmap f (Node x ts) = Node (f x) $ fmap (fmap f) ts

-- Не все типы могут быть функторами
-- например, не получится построить функцию такого типа:
-- fmap :: (a -> b) -> (a -> r) -> (b -> r)

-- Длина списка (для тестирования)
lenWrong :: [Int] -> Int
lenWrong [] = 0
lenWrong (a : as) = (if len as > 5 then a else 1) + len as

-- Тип-дерево (для теста)
data BinTree a
  = Null                           -- Пустое дерево
  | Tree (BinTree a) a (BinTree a) -- Промежуточный узел с поддеревьями
  deriving (Show,Read)          -- Стандартные экземпляры стандартных классов

sumBinTree :: Num a => BinTree a -> a
sumBinTree Null = 0
sumBinTree (Tree left root right) = sumBinTree left + root

